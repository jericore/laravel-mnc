<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'App\Http\Controllers\ProdukController@indexCms')->name('index-grid-produk');
Route::get('grid-produk', 'App\Http\Controllers\ProdukController@indexCms')->name('grid-produk');
Route::get('data-produk', 'App\Http\Controllers\ProdukController@dataProduk')->name('data-produk');
Route::get('tambah-produk', 'App\Http\Controllers\ProdukController@tambahProduk')->name('tambah-produk');
Route::get('edit/{id}', 'App\Http\Controllers\ProdukController@editCMS')->name('edit-produk');
Route::POST('edit/{id}', 'App\Http\Controllers\ProdukController@updateCMS')->name('update-produk');
Route::delete('delete-produk/{id}', 'App\Http\Controllers\ProdukController@destroyCMS')->name('delete-produk');
