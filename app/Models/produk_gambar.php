<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class produk_gambar extends Model
{
    use SoftDeletes;

    public $table = 'produk_gambar';

    public function produk()
    {
        return $this->belongsTo(produk::class, 'produk_id','id');
    }

    use HasFactory;
}
