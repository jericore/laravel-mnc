<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class produk extends Model
{
    use SoftDeletes;

    use HasFactory;

    public function galleries()
    {
        return $this->hasMany(produk_gambar::class, 'produk_id','id');
    }

    public function ukuranHarga()
    {
        return $this->hasMany(produk_ukuran::class, 'produk_id','id');
    }
}
