<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class produk_ukuran extends Model
{
    use SoftDeletes;

    public $table = 'produk_ukuran';

    use HasFactory;
}
