<?php

namespace App\Http\Controllers;

use App\Models\produk;
use App\Models\produk_ukuran;
use App\Models\produk_gambar;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use GuzzleHttp\Client;

class ProdukController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $kategori = request('kategori');
        $getProduk = produk::with(['galleries', 'ukuranHarga'])
        ->when(!empty($kategori), function($query) use($kategori){
            return $query->where('produks.kategori', $kategori);
        })->get();

        if ($getProduk->isEmpty()) {
            return response()->json(['data' => [], 'message' => 'fail',  'error' => true], 404);
        }

        return response()->json(['data' => $getProduk, 'message' => 'success',  'error' => false]);
    }

    public function indexCms(Request $request)
    {
        $kategori = request('filter_kategori');
        $getProduk = produk::with(['galleries', 'ukuranHarga'])
        ->when(!empty($kategori), function($query) use($kategori){
            return $query->where('produks.kategori', $kategori);
        })->get();
        return view('produk.index',  compact('getProduk'));
    }

    public function tambahProduk()
    {
        return view('produk.input');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'data_nama' => 'required',
            'data_kategori' => 'required',
            'data_deskripsi' => 'required',
            'data_ukuran' => 'required',
            'data_harga' => 'required',
            'data_warna' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => 'Failed' ,'state'=>'100' , 'message'=> $validator->errors() ], 422);
        }

        if ($request->file('files') == null) {
            return response()->json(['status' => 'Failed' , 'message'=> "Image Kosong" ], 401);
        }
        
        $dataImage=($request->file('files'));
        $file = count($request->file('files') );
 
        $produk = new produk;
        $produk->nama = $request->data_nama; 
        $produk->kategori = $request->data_kategori;
        $produk->deskripsi = $request->data_deskripsi;
        $produk->warna = $request->data_warna;
        $produk->created_by	= "user"; //harusnya di isi dengan user yang login
        $produk->save();

        $arrayUkuran = explode(",",$request->data_ukuran);
        $arrayHarga = explode(",",$request->data_harga);

        foreach ($arrayUkuran as $key => $value) {
            $produk_ukuran = new produk_ukuran;
            $produk_ukuran->produk_id = $produk->id;
            $produk_ukuran->ukuran = $value;
            $produk_ukuran->harga = $arrayHarga[$key]; 
            $produk_ukuran->created_by	= "user"; //harusnya di isi dengan user yang login
            $produk_ukuran->save();
        }


        $file_ary = array();
        for ($i=0; $i<$file; $i++) { 

            $file_ary[$i]['image'] = $dataImage[$i]->store(
                'assets/gallery', 'public'
            );

            $gallery = new produk_gambar;
            $gallery->produk_id = $produk->id;
            $gallery->gambar = $file_ary[$i]['image'];
            $gallery->created_by	= "user";
            $gallery->save();
        }

        return response()->json(['status' => 'success', 'message' => 'Insert berhasil',  'error' => false]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\produk  $produk
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $showProduct = produk::with(['galleries', 'ukuranHarga'])->where('id', $id)->first();
        if (empty($showProduct)) {
            return response()->json(['data' => [], 'message' => 'Product Not Found',  'error' => true], 404);
        }
        return response()->json(['data' => $showProduct, 'message' => 'success',  'error' => false]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\produk  $produk
     * @return \Illuminate\Http\Response
     */
    public function edit(produk $produk)
    {
        //
    }

    public function editCMS($id)
    {
        $editProduk = produk::with(['galleries', 'ukuranHarga'])->where('id', $id)->first();
        if (empty($editProduk)) {
            return response()->json(['data' => [], 'message' => 'Product Not Found',  'error' => true], 404);
        }
        return view('produk.edit',  compact('editProduk'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\produk  $produk
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, produk $produk)
    {
        //
    }

    public function updateCMS(Request $request, $id)
    {
        dd('develop');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\produk  $produk
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $today = date("Y-m-d g:i:s");
        $showProduct = produk::with(['galleries', 'ukuranHarga'])->where('id', $id)->first();
        if (empty($showProduct)) {
            return response()->json(['data' => [], 'message' => 'Produk not found',  'error' => true], 404);
        }
        $showProduct->delete();
        $delGambar = produk_gambar::where('produk_id', $id)->update(['deleted_at' => $today]);
        $delUkuran = produk_ukuran::where('produk_id', $id)->update(['deleted_at' => $today]);
        return response()->json(['message' => 'delete success',  'error' => false]);

    }

    public function destroyCMS($id)
    {
        $today = date("Y-m-d g:i:s");
        $showProduct = produk::with(['galleries', 'ukuranHarga'])->where('id', $id)->first();
        if (empty($showProduct)) {
            return response()->json(['data' => [], 'message' => 'Produk not found',  'error' => true], 404);
        }
        $showProduct->delete();
        $delGambar = produk_gambar::where('produk_id', $id)->update(['deleted_at' => $today]);
        $delUkuran = produk_ukuran::where('produk_id', $id)->update(['deleted_at' => $today]);
        return redirect(route('index-grid-produk'));

    }
}
