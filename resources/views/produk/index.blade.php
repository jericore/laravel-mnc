@extends('dashboard.master')

@section('content')


<div class="container">
    <div class="row">
       <div class="col-md-12">
           <div class="card">
               <div class="card-header">
                   <h5 class="title">Filter</h5>
               </div>
               <div class="card-body">
                   <form action="{{ route('index-grid-produk') }}">
                    @csrf
                       <div class="row">
                           <div class="col-md-3">
                               <div class="form-group">
                                   <label>Kategori</label>
                                   <input type="text" class="form-control" placeholder="Kategori" value="" name="filter_kategori" id="filter_kategori">
                               </div>
                           </div>
                       </div>
               </div>
               <div class="button-container" style="margin-bottom: 6px; text-align: center;">
                   <button class="btn btn-fill btn-primary">Filter</button>
                   <a href="{{ url('/') }}" class="btn btn-danger">Reset</a>
               </div>
            </form>
           </div>
       </div>
       <div class="col-md-12">
           <div class="card">
               <div class="card-header">
                   
                       <a class="btn btn-fill btn-success"  href="{{ route('tambah-produk') }}" style="float: right;">Tambah</a>
   
                       <h5 class="title">List</h5>
               </div>
               <div class="card-body">
                   <table id="example" class="table table-striped table-bordered" style="width: 100%;">
                       <thead>
                           <tr style="background-color: #3B88C5; color: #FFFFFF;">
                               <th>No</th>
                               <th>Nama</th>
                               <th>Kategori</th>
                               <th>Deskripsi</th>
                               <th>Gambar</th>
                               <th>Warna</th>
                               <th>Ukuran</th>
                               <th>Action</th>
                           </tr>
                       </thead>
                       <tbody>
                        <?php 
                        $nmr = 1;
                        ?>
                        @foreach($getProduk as $value)
                        <tr>
                            <td><?php echo $nmr++; ?></td>
                            <td>{{ $value->nama }}</td>
                            <td>{{ $value->kategori }}</td>
                            <td>{{ $value->deskripsi }}</td>
                            <td>
                                @foreach ($value->galleries as $image)
                                <img src="{{ Storage::url($image->gambar) }}" alt="" style="width: 150px" class="img-thumbnail">
                                @endforeach
                            </td>
                            <td>{{ $value->	warna }}</td>
                            <td>
                                
                                @foreach ($value->ukuranHarga as $record)
                                <option>   {{ $record->ukuran }} : {{ $record->harga }} </option>
                                @endforeach
                                
                            </td>
                            <td>
                                <a href="{{ route('edit-produk', $value->id) }}" class="btn btn-info">Edit</a>
                                <form action="{{ route('delete-produk', $value->id) }}" method="POST" class="d-inline">
                                    @csrf
                                    @method('delete')
                                        <button class="btn btn-danger" onclick="return confirm('Apakah Anda yakin untuk menghapus Produk ini ?')">
                                            <i class="fa fa-trash"></i>
                                        </button>
                                    </form>
                            </td>
                        </tr>
                    @endforeach
                        
                       </tbody>
                       <tfoot>
                       </tfoot>
                   </table>
               </div>
           </div>
       </div>
   </div> 
</div>

@endsection

@section('javascript')
<script type="text/javascript">
    $(document).ready(function () {


        $('#example').DataTable({
            "dom": 'Bfrtip',        
            "buttons": {
            "dom": {
                "button": {
                    "tag": "button",
                    "className": "waves-effect waves-light btn mrm"
                }
            },
            "buttons": ['excelHtml5','pdfHtml5' ]   
            }
        });

        function showCustomer(){ 
            $.get("{{ URL::to('data-produk') }}", function(data){ 
                $('#customerBody').empty().html(data);
            })
        }

    });
    </script>
@endsection