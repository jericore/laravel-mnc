@extends('dashboard.master')

@section('content')

<div class="container">
    <form id="form_1" method="POST" action="{{ route('update-produk', $editProduk->id) }}" enctype="multipart/form-data">
        @csrf
        <input type="hidden" name="submit_mode" id="submit_mode" value="">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h5 class="title">Edit Produk</h5>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="mandatory">Nama</label>
                                    <input type="text" class="form-control" placeholder="Nama" value="{{ $editProduk->nama}}" name="input_nama" id="input_nama">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="mandatory">Kategori</label>
                                    <input class="form-control" placeholder="Kategori" value="{{ $editProduk->kategori}}" name="input_kategori" id="input_kategori">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="mandatory">Deskripsi</label>
                                    <textarea class="form-control" placeholder="Deskripsi" name="input_deskripsi" id="input_deskripsi">{{ $editProduk->deskripsi}}</textarea>
                                </div>
                            </div>

                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
<div class="button-container" style="margin-bottom: 6px; text-align: center;">
    <button class="btn btn-fill btn-success" onclick="submitForm('');">Update</button>
    <a href="{{ url('/') }}" class="btn btn-danger">Kembali</a>
</div>

@endsection

@section('javascript')
<script type="text/javascript">

    function submitForm(){


        var form_data = new FormData();
        form_data.append("data_deskripsi",  $('#input_deskripsi').val());
        form_data.append("data_nama",  $('#input_nama').val());
        form_data.append("data_kategori",  $('#input_kategori').val());


        $.ajax({
            type: "POST",
            processData: false,
            contentType: false,
            url: "{{ route('update-produk', $editProduk->id) }}",
            data: form_data,
            dataType: "json",
            timeout: 300000
        }).done(function(data){
            if(data.status == "success"){
                window.location.href = "{{URL::to('grid-produk')}}";
            }else{

            }
        }).fail(function(data){

        });
    }

    function addRow(){
        document.getElementById('ukuran_harga').innerHTML += '<input type="text" class="form-control" placeholder="Ukuran" value="" name="input_ukuran[]"><input type="text" class="form-control" placeholder="Harga" name="input_harga[]" />' ;
      }
</script>
@endsection