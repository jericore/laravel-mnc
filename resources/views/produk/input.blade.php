@extends('dashboard.master')

@section('content')

<div class="container">
    <form id="form_1" method="POST" action="" enctype="multipart/form-data">
        @csrf
        <input type="hidden" name="submit_mode" id="submit_mode" value="">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h5 class="title">Tambah Produk</h5>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="mandatory">Nama</label>
                                    <input type="text" class="form-control" placeholder="Nama" value="" name="input_nama" id="input_nama">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="mandatory">Kategori</label>
                                    <input class="form-control" placeholder="Kategori" name="input_kategori" id="input_kategori">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="mandatory">Deskripsi</label>
                                    <textarea class="form-control" placeholder="Deskripsi" name="input_deskripsi" id="input_deskripsi"></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="image">Image</label>
                                <br>
                                <input type="file" name="files[]" id="files" multiple/>
                                <input type="hidden" id="img-base64-data_image_thumbnail">
                            </div>  
                            <div class="form-group">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Warna Produk</label>
                                        <div id="ext-combo-warna-produk" class="ext-combo"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <label class="mandatory">Ukuran</label>
                                <div class="form-group" id="ukuran_harga">
                                    <input type="text" class="form-control" placeholder="Ukuran" value="" name="input_ukuran[]">
                                    <input type="text" class="form-control" placeholder="Harga" name="input_harga[]" id="input_harga"/>
                                </div>
                                <a href="#" onclick="addRow();">Tambah Ukuran</a>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
<div class="button-container" style="margin-bottom: 6px; text-align: center;">
    <button class="btn btn-fill btn-success" onclick="submitForm('');">Simpan</button>
    <a href="{{ url('/') }}" class="btn btn-danger">Kembali</a>
</div>

@endsection

@section('javascript')
<script type="text/javascript">

    Ext.onReady(function(){
        Ext.define('StoreModel', {
            extend: 'Ext.data.Model',
            fields: ["id"]
        });
    
        var comboWarnaProduk = Ext.create('Ext.data.Store', {
            fields: ['code', 'name'],
            data : [
                {"code":"Merah"},
                {"code":"Biru"},
                {"code":"Hitam"},
                {"code":"Abu - Abu"}
            ]
        });
        
        Ext.create('Ext.form.field.Tag', {
            id: 'data_warna_produk',
            name: 'data_warna_produk',
            hiddenName: 'ext_data_warna_produk',
            xtype: 'tagfield',
            store: comboWarnaProduk,
            queryMode: 'local',
            displayField: 'code',
            valueField: 'code',
            renderTo: "ext-combo-warna-produk",
            emptyText: 'Kode Status',
            width: '100%',
            filterPickList: true,
            matchFieldWidth: true,
            listConfig:{
                emptyText: '<div class="ext-not-found">Data tidak ditemukan.</div>',
                minWidth: 360
            },
            listeners: {
                select: function(combo, record, opts){

                },
                change: function(combo, newVal, oldVal, opts){
                    if(!newVal){
                        
                    }
                }
            }
        });
    
    
    });

    function submitForm(){

        var tmpTagWarna = [];
        for(var i=0; i<$('[name=ext_data_warna_produk]').length; i++){
            tmpTagWarna.push($('[name=ext_data_warna_produk]')[i].value);
        }

        if(tmpTagWarna){
            tmpTagWarna = tmpTagWarna.join();
        }

        var form_data = new FormData();
        var totalfiles = document.getElementById('files').files.length;
        var totalUkuran = $("input[name='input_ukuran[]']")
              .map(function(){return $(this).val();}).get();
        var dataHarga = $("input[name='input_harga[]']")
              .map(function(){return $(this).val();}).get();
        for (var index = 0; index < totalfiles; index++) {
            form_data.append("files[]", document.getElementById('files').files[index]);
        }

        form_data.append("data_deskripsi",  $('#input_deskripsi').val());
        form_data.append("data_deskripsi",  $('#input_deskripsi').val());
        form_data.append("data_nama",  $('#input_nama').val());
        form_data.append("data_kategori",  $('#input_kategori').val());
        form_data.append("data_warna",  tmpTagWarna);
        form_data.append("data_ukuran",  totalUkuran);
        form_data.append("data_harga",  dataHarga);


        $.ajax({
            type: "POST",
            processData: false,
            contentType: false,
            url: "{{ env('URL_API') }}"+"/api/produk",
            data: form_data,
            dataType: "json",
            timeout: 300000
        }).done(function(data){
            if(data.status == "success"){
                window.location.href = "{{URL::to('grid-produk')}}";
            }else{

            }
        }).fail(function(data){

        });
    }

    function addRow(){
        document.getElementById('ukuran_harga').innerHTML += '<input type="text" class="form-control" placeholder="Ukuran" value="" name="input_ukuran[]"><input type="text" class="form-control" placeholder="Harga" name="input_harga[]" />' ;
      }
</script>
@endsection